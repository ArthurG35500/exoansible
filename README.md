# Configuration de VM avec Ansible

Ce projet utilise Ansible pour configurer automatiquement une VM Ubuntu, en installant plusieurs paquets utiles et en sécurisant la configuration SSH.

## Fonctionnalités

- Mise à jour de tous les paquets existants.
- Installation et configuration d'UFW (Uncomplicated Firewall).
- Installation de Python 3 et Pip.
- Installation d'Ansible via Pip.
- Installation automatique de Lazydocker si non présent.
- Ajout de Lazydocker au `PATH` de l'utilisateur `ansible`.
- Désactivation de la connexion SSH pour l'utilisateur root.
- Désactivation de la connexion SSH via mot de passe.
- Redémarrage du service SSH pour appliquer les modifications de configuration.

## Prérequis

- Une VM Ubuntu 22.04.
- Ansible installé sur votre machine de contrôle.
- Accès SSH configuré pour l'utilisateur `ansible`.
